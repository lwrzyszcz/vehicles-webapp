import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatNativeDateModule, MatPaginatorModule,
  MatSelectModule,
  MatTableModule
} from '@angular/material';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [
    MatInputModule, MatButtonModule, MatCheckboxModule, MatSelectModule, MatDatepickerModule, MatTableModule, MatNativeDateModule, MatDialogModule, MatPaginatorModule,
  ],
  exports: [
    MatInputModule, MatButtonModule, MatCheckboxModule, MatSelectModule, MatDatepickerModule, MatTableModule, MatNativeDateModule, MatDialogModule, MatPaginatorModule
  ],
  declarations: []
})
export class MaterialDesignModule {
}
