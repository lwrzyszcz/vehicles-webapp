import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesBorrowsComponent } from './vehicles-borrows.component';

describe('VehiclesBorrowsComponent', () => {
  let component: VehiclesBorrowsComponent;
  let fixture: ComponentFixture<VehiclesBorrowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclesBorrowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesBorrowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
