import {Component, Input, OnInit} from '@angular/core';
import {AbstractForm} from '../../../utils/abstract-form';
import {ProducersService} from '../../../services/producers.service';

@Component({
  selector: 'app-bike-form',
  templateUrl: './bike-form.component.html',
  styleUrls: ['./bike-form.component.scss']
})
export class BikeFormComponent extends AbstractForm implements OnInit {
  @Input()
  element = {};
  @Input()
  disabled = false;

  producers = [];
  constructor(private producersService: ProducersService) {
    super();
  }

  ngOnInit() {
    this.producersService.getAllProducers().then(resp => {
      this.producers = resp;
      if (this.disabled) {
        this.formGroup.disable();
      }
    });
  }

}
