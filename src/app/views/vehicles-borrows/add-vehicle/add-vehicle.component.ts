import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {VehiclesService} from '../../services/vehicles.service';
import {BikeFormComponent} from './bike-form/bike-form.component';
import {VehicleModel} from '../../models/vehicle';
import {CarFormComponent} from './car-form/car-form.component';

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss']
})
export class AddVehicleComponent implements OnInit {

  @ViewChild('bikeForm') bikeForm: BikeFormComponent;
  @ViewChild('carForm') carForm: CarFormComponent;

  type = 'Car';

  element = {};

  editMode = false;

  constructor(public dialogRef: MatDialogRef<AddVehicleComponent>,
              @Inject(MAT_DIALOG_DATA) public data: VehicleModel,
              private vehicleService: VehiclesService) {
    if (data !== null) {
      this.editMode = true;
      this.element = {
        id: data.id,
        name: data.name,
        color: data.color,
        productionDate: new Date(data.productionDate)
      };
      if (this.data.producer !== null) {
        this.element['producerId'] = this.data.producer.id;
      }
    }
  }

  ngOnInit() {

  }

  submit() {
    if (this.type === 'Car' && this.carForm.formGroup.valid) {
      if (!this.editMode) {
        this.addCar();
      } else {
        this.editCar();
      }
    } else if (this.type === 'Bike' && this.bikeForm.formGroup.valid) {
      this.addBike();
    }
  }

  private addCar() {
    this.carForm.formGroup.updateValueAndValidity();
    this.vehicleService.addCar(this.carForm.formGroup.value).then(resp => {
      this.dialogRef.close();
    });
  }

  private editCar() {
    this.carForm.formGroup.updateValueAndValidity();
    const value = this.carForm.formGroup.value;
    value['id'] = this.element['id'];
    this.vehicleService.editCar(value).then(resp => {
      this.dialogRef.close();
    });
  }

  private addBike() {
    this.bikeForm.formGroup.updateValueAndValidity();
    this.vehicleService.addBikeWithNumber(this.bikeForm.formGroup.value.bikeNumber).then(resp => {
      this.dialogRef.close();
    });
  }
}
