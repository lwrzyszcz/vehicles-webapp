import {Component, Input, OnInit} from '@angular/core';
import {AbstractForm} from '../../../utils/abstract-form';
import {ProducersService} from '../../../services/producers.service';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.scss']
})
export class CarFormComponent extends AbstractForm implements OnInit {

  @Input()
  element;
  @Input()
  disabled = false;
  producers = [];

  constructor(private producersService: ProducersService) {
    super();
  }

  ngOnInit() {
    this.producersService.getAllProducers().then(resp => {
      this.producers = resp;
      if (this.disabled) {
        this.formGroup.disable();
      }
    });
    this.getControlForField('name', this.element).setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(20)]);
  }

}
