import {Component, OnInit} from '@angular/core';
import {BorrowsService} from '../services/borrows.service';
import {MatDialog, PageEvent} from '@angular/material';
import {AddVehicleComponent} from './add-vehicle/add-vehicle.component';
import {VehiclesService} from '../services/vehicles.service';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {Router} from '@angular/router';
import {VehicleModel} from '../models/vehicle';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-vehicles-borrows',
  templateUrl: './vehicles-borrows.component.html',
  styleUrls: ['./vehicles-borrows.component.scss']
})
export class VehiclesBorrowsComponent implements OnInit {

  dataSource;

  dateControl = new FormControl();

  displayedColumns: string[] = ['type', 'vehicleName', 'producerName', 'productionDate', 'color', 'borrowerName', 'borrowDate', 'actions'];
  date = new Date();

  paging = {
    pageNumber: 0,
    pageSize: 10,
    offset: 0
  };

  constructor(private borrowsService: BorrowsService, private dialog: MatDialog, private vehicleService: VehiclesService, private router: Router) {
    this.getBorrows();
  }

  ngOnInit() {
    this.dateControl.setValue(this.date);
  }

  onDateChange(event) {
    this.date = event.value;
    this.dateControl.setValue(this.date);
    this.getBorrows();
  }

  onAdd() {
    this.dialog.open(AddVehicleComponent, {disableClose: true}).afterClosed().subscribe(() => {
      this.getBorrows();
    });
  }

  onEdit(element: VehicleModel) {
    this.dialog.open(AddVehicleComponent, {disableClose: true, data: element}).afterClosed().subscribe(() => {
      this.getBorrows();
    });
  }

  onDelete(element: VehicleModel) {
    this.dialog.open(ConfirmationComponent, {
      disableClose: true,
      data: 'Czy jestes pewny, ze chcesz usunac ten element?'
    }).afterClosed().subscribe((data) => {
      if (data) {
        this.vehicleService.deleteVehicle(element.id).then(() => {
          this.getBorrows();
        });
      }
    });
  }

  showDetails(element: VehicleModel) {
    this.router.navigate(['details', element.id]);
  }

  onPagingChange(paging: PageEvent) {
    this.paging.pageNumber = paging.pageIndex;
    this.paging.pageSize = paging.pageSize;
    this.paging.offset = paging.pageIndex * paging.pageSize;
    this.getBorrows();
  }

  private getBorrows() {
    this.borrowsService.getBorrowsInDay(this.date, JSON.stringify(this.paging)).then(resp => {
      this.dataSource = resp;
    });
  }

}
