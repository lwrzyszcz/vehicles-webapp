import {Injectable} from '@angular/core';
import {RestApiService} from '../../services/rest-api.service';
import {ProducerModel} from '../models/producer';

@Injectable({
  providedIn: 'root'
})
export class ProducersService {

  constructor(private restApi: RestApiService) {
  }

  getAllProducers(): Promise<any> {
    return this.restApi.get(`/allProducers`);

  }

  addProducer(producer: ProducerModel): Promise<any> {
    return this.restApi.post('/addProducer', producer);
  }

  deleteProducer(id: number): Promise<any> {
    return this.restApi.delete(`/deleteProducer/${id}`);
  }
}
