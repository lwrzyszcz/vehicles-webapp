import {Injectable} from '@angular/core';
import {RestApiService} from '../../services/rest-api.service';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(private restApi: RestApiService) {
  }

  addCar(vehicle): Promise<any> {
    vehicle.productionDate = new DatePipe('en-US').transform(vehicle.productionDate.toLocaleDateString(), 'yyy-MM-dd');
    return this.restApi.post('/addCar', vehicle);
  }

  editCar(vehicle): Promise<any> {
    vehicle.productionDate = new DatePipe('en-US').transform(vehicle.productionDate.toLocaleDateString(), 'yyy-MM-dd');
    return this.restApi.put('/editCar', vehicle);
  }

  deleteVehicle(id): Promise<any> {
    return this.restApi.delete(`/delete/${id}`);
  }

  getCarById(id): Promise<any> {
    return this.restApi.get(`/details/${id}`);
  }

  addBikeWithNumber(number): Promise<any> {
    return this.restApi.post(`/addBike/${number}`, {});
  }
}
