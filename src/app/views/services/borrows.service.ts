import {Injectable} from '@angular/core';
import {RestApiService} from '../../services/rest-api.service';
import {DatePipe} from '@angular/common';
import {BorrowerModel} from '../models/borrower';

@Injectable({
  providedIn: 'root'
})
export class BorrowsService {

  constructor(private restApi: RestApiService) {
  }

  getBorrowsInDay(date: Date, paging: string): Promise<any> {
    const stringDate = new DatePipe('en-US').transform(date.toLocaleDateString(), 'yyy-MM-dd');
    return this.restApi.get(`/show/${stringDate}/${paging}`);

  }

  borrowVehicle(borrow): Promise<any> {
    const stringDate = new DatePipe('en-US').transform(borrow.borrowDate.toLocaleDateString(), 'yyy-MM-dd');
    borrow.borrowDate = stringDate;
    return this.restApi.post('/borrow', borrow);
  }

  addBorrower(borrower: BorrowerModel): Promise<any> {
    return this.restApi.post('/addBorrower', borrower);
  }


  getBorrowers(): Promise<any> {
    return this.restApi.get('/allBorrowers');
  }

  deleteBorrower(id: number): Promise<any> {
    return this.restApi.delete(`/deleteBorrower/${id}`);
  }

  getBorrows(): Promise<any> {
    return this.restApi.get('/allBorrows');
  }

  deleteBorrow(id: number): Promise<any> {
    return this.restApi.delete(`/deleteBorrow/${id}`);
  }
}
