import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VehiclesBorrowsComponent} from './vehicles-borrows/vehicles-borrows.component';
import {StartViewComponent} from './start-view/start-view.component';
import {VehicleDetailsComponent} from './vehicle-details/vehicle-details.component';
import {ProducersComponent} from './producers/producers.component';
import {BorrowersComponent} from './borrowers/borrowers.component';
import {BorrowsComponent} from './borrows/borrows.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'start', component: StartViewComponent},
  {path: 'borrows', component: VehiclesBorrowsComponent},
  {path: 'details/:id', component: VehicleDetailsComponent},
  {path: 'producers', component: ProducersComponent},
  {path: 'borrowers', component: BorrowersComponent},
  {path: 'borrows-history', component: BorrowsComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class ViewsRoutingModule {
}
