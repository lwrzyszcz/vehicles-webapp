import {FormControl, FormGroup} from '@angular/forms';
import {isNullOrUndefined} from 'util';

export abstract class AbstractForm {

  formGroup = new FormGroup({});

  private controls: Map<string, FormControl> = new Map<string, FormControl>();

  getControlForField(field: string, value = {}): FormControl {
    let control = this.controls.get(field);
    if (control === undefined) {
      control = new FormControl();
      this.formGroup.addControl(field, control);
      this.controls.set(field, control);
      if (value !== null && value !== undefined) {
        control.setValue(value[field]);
      }
    }
    return control;
  }

  getErrorInControl(controlName: string) {
    const errors = this.getControlForField(controlName).errors;
    if (isNullOrUndefined(errors)) {
      return undefined;
    }
    return Object.keys(errors)[0];
  }
}
