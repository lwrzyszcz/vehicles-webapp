import {Component, OnInit} from '@angular/core';

import {MatDialog} from '@angular/material';

import {ConfirmationComponent} from '../vehicles-borrows/confirmation/confirmation.component';
import {BorrowsService} from '../services/borrows.service';

@Component({
  selector: 'app-borrows',
  templateUrl: './borrows.component.html',
  styleUrls: ['./borrows.component.scss']
})
export class BorrowsComponent implements OnInit {

  displayedColumns: string[] = ['date', 'name', 'producerName', 'vehicleName', 'actions'];

  dataSource;

  constructor(private dialog: MatDialog, private borrowsService: BorrowsService) {
  }

  ngOnInit() {
    this.getBorrows();
  }

  onDelete(element) {
    this.dialog.open(ConfirmationComponent, {
      disableClose: true,
      data: 'Czy jestes pewny, ze chcesz usunac ten element?'
    }).afterClosed().subscribe((data) => {
      if (data) {
        this.deleteBorrow(element);
      }
    });
  }

  private deleteBorrow(element ) {
    this.borrowsService.deleteBorrow(element.id).then(() => {
      this.getBorrows();
    }).catch(e => {
      this.dialog.open(ConfirmationComponent, {
        disableClose: true,
        data: 'Nie mozna usunac elementu. ' +
          '\n' +
          e.message
      });
    });
  }

  private getBorrows() {
    this.borrowsService.getBorrows().then(resp => {
      this.dataSource = resp;
    });
  }
}
