import {BorrowerModel} from './borrower';

export interface BorrowModel {
  borrowDate: string;
  vehicleId: number;
  borrower: BorrowerModel;
}
