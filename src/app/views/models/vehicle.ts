import {ProducerModel} from './producer';
import {BorrowerModel} from './borrower';

export interface VehicleModel {
  id: number;

  bikeNumber: number;

  name: string;

  color: string;

  producer: ProducerModel;

  borrow: BorrowerModel;

  productionDate: string;

  type: string;

  borrowed: boolean;
}
