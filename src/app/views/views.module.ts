import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VehiclesBorrowsComponent} from './vehicles-borrows/vehicles-borrows.component';
import {MaterialDesignModule} from '../material-design/material-design.module';
import {ViewsRoutingModule} from './views-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ServicesModule} from '../services/services.module';
import {BorrowsService} from './services/borrows.service';
import {AddVehicleComponent} from './vehicles-borrows/add-vehicle/add-vehicle.component';
import {ProducersService} from './services/producers.service';
import {VehiclesService} from './services/vehicles.service';
import {BikeFormComponent} from './vehicles-borrows/add-vehicle/bike-form/bike-form.component';
import {StartViewComponent} from './start-view/start-view.component';
import {RouterModule} from '@angular/router';
import {ConfirmationComponent} from './vehicles-borrows/confirmation/confirmation.component';
import {VehicleDetailsComponent} from './vehicle-details/vehicle-details.component';
import {CarFormComponent} from './vehicles-borrows/add-vehicle/car-form/car-form.component';
import {BorrowComponent} from './vehicle-details/borrow/borrow.component';
import {ProducersComponent} from './producers/producers.component';
import {ProducerFormComponent} from './producers/producer-form/producer-form.component';
import {BorrowersComponent} from './borrowers/borrowers.component';
import {BorrowersFormComponent} from './borrowers/borrowers-form/borrowers-form.component';
import {BorrowsComponent} from './borrows/borrows.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialDesignModule,
    RouterModule,
    ViewsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ServicesModule
  ],
  declarations: [
    VehiclesBorrowsComponent,
    AddVehicleComponent,
    BikeFormComponent,
    StartViewComponent,
    ConfirmationComponent,
    VehicleDetailsComponent,
    CarFormComponent,
    BorrowComponent,
    ProducersComponent,
    ProducerFormComponent,
    BorrowersComponent,
    BorrowersFormComponent,
    BorrowsComponent,
    LoginComponent
  ],
  providers: [BorrowsService, ProducersService, VehiclesService],
  entryComponents: [AddVehicleComponent, ConfirmationComponent, BorrowComponent, ProducerFormComponent, BorrowersFormComponent]
})
export class ViewsModule {
}
