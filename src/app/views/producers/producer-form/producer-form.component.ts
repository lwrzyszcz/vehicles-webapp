import {Component, Inject, Input, OnInit} from '@angular/core';
import {AbstractForm} from '../../utils/abstract-form';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProducerModel} from '../../models/producer';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-producer-form',
  templateUrl: './producer-form.component.html',
  styleUrls: ['./producer-form.component.scss']
})
export class ProducerFormComponent extends AbstractForm implements OnInit {


  element;

  constructor(public dialogRef: MatDialogRef<ProducerFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ProducerModel) {
    super();
  }

  ngOnInit() {
    this.element = this.data;
    this.getControlForField('name', this.element).setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(20)]);

  }

}
