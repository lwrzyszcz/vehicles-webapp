import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProducerFormComponent } from './producer-form.component';

describe('BorrowsFormComponent', () => {
  let component: ProducerFormComponent;
  let fixture: ComponentFixture<ProducerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProducerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProducerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
