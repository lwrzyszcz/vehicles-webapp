import {Component, OnInit} from '@angular/core';
import {ProducersService} from '../services/producers.service';
import {VehicleModel} from '../models/vehicle';
import {MatDialog} from '@angular/material';
import {ProducerFormComponent} from './producer-form/producer-form.component';
import {ConfirmationComponent} from '../vehicles-borrows/confirmation/confirmation.component';

@Component({
  selector: 'app-producers',
  templateUrl: './producers.component.html',
  styleUrls: ['./producers.component.scss']
})
export class ProducersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'actions'];

  dataSource;

  constructor(private dialog: MatDialog, private producersService: ProducersService) {
  }

  ngOnInit() {
    this.getProducers();
  }

  onAdd() {
    this.dialog.open(ProducerFormComponent, {disableClose: true}).afterClosed().subscribe((producer) => {
      if (producer) {
        this.addProducer(producer);
      }
    });
  }

  onEdit(element: VehicleModel) {
    this.dialog.open(ProducerFormComponent, {disableClose: true, data: element}).afterClosed().subscribe((producer) => {
      if (producer) {
        element.name = producer.name;
        this.addProducer(element);
      }
    });
  }

  onDelete(element: VehicleModel) {
    this.dialog.open(ConfirmationComponent, {
      disableClose: true,
      data: 'Czy jestes pewny, ze chcesz usunac ten element?'
    }).afterClosed().subscribe((data) => {
      if (data) {
        this.deleteProducer(element);
      }
    });
  }

  private deleteProducer(element: VehicleModel) {
    this.producersService.deleteProducer(element.id).then(() => {
      this.getProducers();
    }).catch(e => {
      this.dialog.open(ConfirmationComponent, {
        disableClose: true,
        data: 'Nie mozna usunac elementu. ' +
          '\n' +
          e.message
      });
    });
  }

  private addProducer(producer) {
    this.producersService.addProducer(producer).then(resp => {
      this.getProducers();
    });
  }

  private getProducers() {
    this.producersService.getAllProducers().then(resp => {
      this.dataSource = resp;
    });
  }
}
