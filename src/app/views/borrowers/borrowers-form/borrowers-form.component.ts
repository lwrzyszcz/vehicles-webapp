import {Component, Inject, Input, OnInit} from '@angular/core';
import {AbstractForm} from '../../utils/abstract-form';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {VehicleModel} from '../../models/vehicle';
import {BorrowerModel} from '../../models/borrower';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-borrowers-form',
  templateUrl: './borrowers-form.component.html',
  styleUrls: ['./borrowers-form.component.scss']
})
export class BorrowersFormComponent extends AbstractForm implements OnInit {


  element;

  constructor(public dialogRef: MatDialogRef<BorrowersFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: BorrowerModel) {
    super();
  }

  ngOnInit() {
    this.element = this.data;
    this.getControlForField('name', this.element).setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(20)]);

  }

}
