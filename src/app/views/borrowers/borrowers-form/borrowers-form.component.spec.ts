import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowersFormComponent } from './borrowers-form.component';

describe('BorrowsFormComponent', () => {
  let component: BorrowersFormComponent;
  let fixture: ComponentFixture<BorrowersFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowersFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowersFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
