import {Component, OnInit} from '@angular/core';

import {MatDialog} from '@angular/material';
import {BorrowersFormComponent} from './borrowers-form/borrowers-form.component';
import {ConfirmationComponent} from '../vehicles-borrows/confirmation/confirmation.component';
import {BorrowerModel} from '../models/borrower';
import {BorrowsService} from '../services/borrows.service';

@Component({
  selector: 'app-borrowers',
  templateUrl: './borrowers.component.html',
  styleUrls: ['./borrowers.component.scss']
})
export class BorrowersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'actions'];

  dataSource;

  constructor(private dialog: MatDialog, private borrowsService: BorrowsService) {
  }

  ngOnInit() {
    this.getBorrowers();
  }

  onAdd() {
    this.dialog.open(BorrowersFormComponent, {disableClose: true}).afterClosed().subscribe((borrower) => {
      if (borrower) {
        this.addBorrower(borrower);
      }
    });
  }

  onEdit(element: BorrowerModel) {
    this.dialog.open(BorrowersFormComponent, {disableClose: true, data: element}).afterClosed().subscribe((borrower) => {
      if (borrower) {
        element.name = borrower.name;
        this.addBorrower(element);
      }
    });
  }

  onDelete(element: BorrowerModel) {
    this.dialog.open(ConfirmationComponent, {
      disableClose: true,
      data: 'Czy jestes pewny, ze chcesz usunac ten element?'
    }).afterClosed().subscribe((data) => {
      if (data) {
        this.deleteBorrower(element);
      }
    });
  }

  private deleteBorrower(element: BorrowerModel) {
    this.borrowsService.deleteBorrower(element.id).then(() => {
      this.getBorrowers();
    }).catch(e => {
      this.dialog.open(ConfirmationComponent, {
        disableClose: true,
        data: 'Nie mozna usunac elementu. ' +
          '\n' +
          e.message
      });
    });
  }

  private addBorrower(borrower) {
    this.borrowsService.addBorrower(borrower).then(resp => {
      this.getBorrowers();
    });
  }

  private getBorrowers() {
    this.borrowsService.getBorrowers().then(resp => {
      this.dataSource = resp;
    });
  }
}
