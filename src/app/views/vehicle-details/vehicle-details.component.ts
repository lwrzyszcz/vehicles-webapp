import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {VehiclesService} from '../services/vehicles.service';
import {VehicleModel} from '../models/vehicle';
import {BikeFormComponent} from '../vehicles-borrows/add-vehicle/bike-form/bike-form.component';
import {CarFormComponent} from '../vehicles-borrows/add-vehicle/car-form/car-form.component';
import {MatDialog} from '@angular/material';
import {BorrowComponent} from './borrow/borrow.component';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.scss']
})
export class VehicleDetailsComponent implements OnInit {

  @ViewChild('bikeForm') bikeForm: BikeFormComponent;
  @ViewChild('carForm') carForm: CarFormComponent;

  element;
  private vehicle: VehicleModel;


  constructor(private router: Router, private vehiclesService: VehiclesService, private dialog: MatDialog) {
    const split = this.router.url.split('/');
    const id = split[split.length - 1];
    this.vehiclesService.getCarById(id).then(resp => {
      this.vehicle = resp;
      this.setElement();
    });
  }

  ngOnInit() {

  }

  borrow() {
    this.dialog.open(BorrowComponent, {disableClose: true, data: this.vehicle})
  }

  private setElement() {
    this.element = {
      id: this.vehicle.id,
      name: this.vehicle.name,
      color: this.vehicle.color,
      productionDate: new Date(this.vehicle.productionDate),
      type: this.vehicle.type,
      bikeNumber: this.vehicle.bikeNumber
    };

    if (this.vehicle.producer !== null) {
      this.element['producerId'] = this.vehicle.producer.id;
    }
  }
}
