import {Component, Inject, OnInit} from '@angular/core';
import {AbstractForm} from '../../utils/abstract-form';
import {BorrowsService} from '../../services/borrows.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.scss']
})
export class BorrowComponent extends AbstractForm implements OnInit {

  borrowers = [];

  constructor(public dialogRef: MatDialogRef<BorrowComponent>,
              @Inject(MAT_DIALOG_DATA) public data, private borrowService: BorrowsService) {
    super();
    this.borrowService.getBorrowers().then(resp => {
      this.borrowers = resp;
    });
  }

  ngOnInit() {
    this.getControlForField('borrowDate').setValidators([Validators.required]);
    this.getControlForField('borrower').setValidators([Validators.required]);
  }

  submit() {
    this.formGroup.updateValueAndValidity();
    if (this.formGroup.valid) {
      const value = this.formGroup.value;
      value['vehicleId'] = this.data.id;
      this.borrowService.borrowVehicle(value).then(resp => {
        this.dialogRef.close();
      });
    }
  }

}
