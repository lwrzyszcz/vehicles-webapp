import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {RestApiService} from './rest-api.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthenticationService } from './authentication.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  declarations: [],
  providers: [RestApiService, AuthenticationService]
})
export class ServicesModule { }
