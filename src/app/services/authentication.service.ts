import { Injectable } from '@angular/core';
import { OAuthService, AuthConfig, JwksValidationHandler } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  // Url of the Identity Provider
  issuer: 'https://accounts.google.com',

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin + '/start',
  loginUrl: 'https://accounts.google.com/o/oauth2/v2/auth',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: '184779614397-neoierplafgg4a1ct0i908a6q82r7ebi.apps.googleusercontent.com',
  strictDiscoveryDocumentValidation: false,
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile email',
  dummyClientSecret: 'XfaWSjSkMQx37cmUErG2sz4Q'
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private oauthService: OAuthService) {
    this.configureWithNewConfigApi();
  }

  configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.tryLogin();
  }

  tryLogin() {
    this.oauthService.loadDiscoveryDocument().then(() => {
      this.oauthService.tryLogin();
    });
  }

  login() {
    this.oauthService.initImplicitFlowInternal();
  }
}