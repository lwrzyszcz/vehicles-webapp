import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  baseUrl = 'http://localhost:8090';

  constructor(private httpClient: HttpClient, private oauthService: OAuthService) {
  }

  public get(endpoint: string, headers: any = {headers: this.getHeaders()}): Promise<any> {
    return this.httpClient.get(this.baseUrl + endpoint, headers).toPromise();
  }

  public post(endpoint: string, body: any, headers: any = {headers: this.getHeaders()}): Promise<any> {
    return this.httpClient.post(this.baseUrl + endpoint, body, headers).toPromise();
  }

  public put(endpoint: string, body: any, headers: any = {headers: this.getHeaders()}): Promise<any> {
    return this.httpClient.put(this.baseUrl + endpoint, body, headers).toPromise();
  }

  public delete(endpoint: string, headers: any = {headers: this.getHeaders()}): Promise<any> {
    return this.httpClient.delete(this.baseUrl + endpoint, headers).toPromise();
  }

  public getHeaders() {
    return new HttpHeaders({
      'token': this.oauthService.getAccessToken(),
    });
  }
}
